from django.contrib import admin

from apps.timers.models import Timer
from .models import Room


class TimerInline(admin.TabularInline):
    model = Timer
    extra = 1


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    inlines = [TimerInline]
