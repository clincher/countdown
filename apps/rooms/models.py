from django.db import models


class Room(models.Model):
    name = models.CharField('Название', max_length=100)

    class Meta:
        verbose_name = 'Зал'
        verbose_name_plural = 'Залы'

    def __str__(self):
        return self.name
