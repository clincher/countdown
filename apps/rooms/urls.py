from django.urls import path
from django.views.generic import ListView, DetailView

from .models import Room


urlpatterns = [
    path('', ListView.as_view(model=Room), name='home'),
    path('<int:pk>', DetailView.as_view(model=Room), name='room_detail'),
]
