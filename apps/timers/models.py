from django.db import models
from django.utils import timezone


def humanize_time(secs):
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    return '%02dч %02dм %02dс' % (hours, mins, secs)


class Timer(models.Model):
    room = models.ForeignKey(
        'rooms.Room', verbose_name='Зал', on_delete=models.CASCADE)
    name = models.CharField('Название', max_length=100)
    length = models.PositiveIntegerField('Длительность', help_text='в секундах')
    start_time = models.DateTimeField('Начало в', default=timezone.now)
    is_going = models.BooleanField(
        'Запущен', default=False, help_text='отвечает за паузу')
    is_published = models.BooleanField('Показать', default=False)

    class Meta:
        verbose_name = 'Таймер'
        verbose_name_plural = 'Таймеры'
        ordering = ['room', '-start_time']

    def __str__(self):
        return self.name

    def time_left(self):
        return humanize_time(self.length)
