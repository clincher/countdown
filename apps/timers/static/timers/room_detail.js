var interval;
var timer_data = {};
var timer;
$( document ).ready(function() {
    var get_url = $("#content-wrapper").attr('data-get-url');
    initiateTimer = function (timer_data){
        $('#timer-title').text(timer_data['object']['name']);
        $("#timer-container").remove();
        var container = $("<div id='timer-container' data-timer='" + timer_data['object']['length']+ "'></div>");
        $("#content-wrapper").append(container);
        timer = timer_data['object']['length'];
        $("#timer-container").TimeCircles({
            count_past_zero: false,
            circle_bg_color: "#90989F",
            fg_width: 0.03,
            bg_width: 0.2,
            timer: timer,
            start: timer_data['object']['is_going'],
            time: { //  a group of options that allows you to control the options of each time unit independently.
                Days: {
                    show: false,
                    text: "Дни",
                    color: "#ffffff"
                },
                Hours: {
                    show: true,
                    text: "Часы",
                    color: "#ffffff"
                },
                Minutes: {
                    show: true,
                    text: "Минуты",
                    color: "#ffffff"
                },
                Seconds: {
                    show: true,
                    text: "Секунды",
                    color: "#ffffff"
                }
            }
        });
    };
    checkTimer = function (){
        $.get(
            get_url,
            function(data){
                if (data['object'] && timer_data['object']) {
                    if (timer_data['object']['id'] != data['object']['id']) {
                        $("#timer-container").TimeCircles().destroy();
                        timer_data = data;
                        initiateTimer(timer_data)
                    } else  {
                        if (timer_data['object']['is_going'] != data['object']['is_going']) {
                            if (data['object']['is_going']) {
                                $("#timer-container").TimeCircles().start()
                            } else {
                                $("#timer-container").TimeCircles().stop()
                            }
                        }
                    }

                } else {
                    if (data['object']) {
                        timer_data = data;
                        initiateTimer(timer_data)
                    }
                }
            }
        ).fail(function(e) {
            e.preventDefault();
            console.log('unavailable')
        });
    };
    interval = setInterval(checkTimer, 1000);
});
