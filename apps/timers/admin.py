from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse, path
from django.utils.html import format_html

from .models import Timer


@admin.register(Timer)
class TimerAdmin(admin.ModelAdmin):
    list_display = ['name', 'room', 'start_time', 'length', 'time_left',
                    'timer_actions']
    list_editable = ['room', 'start_time', 'length']
    list_filter = ['room']

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path(
                '<int:pk>/start',
                self.admin_site.admin_view(self.start_timer),
                name='start_timer',
            ),
            path(
                '<int:pk>/pause',
                self.admin_site.admin_view(self.pause_timer),
                name='pause_timer',
            ),
        ]
        return custom_urls + urls

    def timer_actions(self, obj):
        if obj.is_going:
            url = reverse('admin:pause_timer', args=[obj.pk])
            action = 'Pause'
        else:
            url = reverse('admin:start_timer', args=[obj.pk])
            action = 'Start'

        return format_html(
            '<a class="button" href="{}">{}</a>&nbsp;', url, action)

    def start_timer(self, request, pk, *args, **kwargs):
        obj = self.get_object(request, pk)
        Timer.objects.filter(room=obj.room).exclude(pk=obj.pk).update(
            is_published=False, is_going=False)
        obj.is_published = True
        obj.is_going = True
        obj.save()
        return HttpResponseRedirect(request.META['HTTP_REFERER'])

    def pause_timer(self, request, pk, *args, **kwargs):
        obj = self.get_object(request, pk)
        obj.is_going = False
        obj.save()
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
