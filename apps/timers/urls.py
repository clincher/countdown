from django.urls import path

from apps.timers.views import GetTimerView


urlpatterns = [
    path('<int:room_pk>', GetTimerView.as_view(), name='get_timer'),
]
