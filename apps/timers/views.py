from collections import defaultdict
from django import http
from django.views import generic

from .models import Timer


class JsonResponseMixin(generic.DetailView):
    def render_to_response(self, context, **response_kwargs):
        data = dict()
        data['object'] = context['object']
        return http.JsonResponse(context)


class GetTimerView(JsonResponseMixin, generic.DetailView):
    model = Timer

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        queryset = queryset.filter(
            room=self.kwargs['room_pk'], is_published=True)
        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            return None
        return obj

    def render_to_response(self, context, **response_kwargs):
        data = defaultdict(dict)
        obj = context['object']
        if obj:
            data['object'] = {
                'name': obj.name,
                'length': obj.length,
                'id': obj.pk,
                'is_going': obj.is_going
            }
        else:
            data['object'] = None
        return http.JsonResponse(data)


class SaveTimerView(JsonResponseMixin, generic.UpdateView):
    fields = ['length']
